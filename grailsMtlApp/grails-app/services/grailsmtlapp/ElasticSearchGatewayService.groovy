package grailsmtlapp

import grails.converters.JSON
import groovy.json.*
import org.apache.http.client.HttpResponseException
import org.apache.http.client.fluent.*
import org.apache.http.conn.ConnectTimeoutException
import org.apache.http.conn.HttpHostConnectException
import org.apache.http.entity.*
import org.codehaus.groovy.grails.web.json.JSONObject

import java.text.SimpleDateFormat

class  ElasticSearchGatewayService {
    final SERVER_URL_ADDRESS = "http://localhost:9200"  //it must be replaced with the actual server url:port

    static final CONTACT_SCHEMA = [
            type: 'object',
            required: true,
            properties: [
                    fournisseur: [type:'string', required:true],
                    service: [type:'string'],
                    montant: [type:'number', required:true, minimum:0],
                    numero: [type:'string', required:true],
                    approbateur: [type:'string'],
                    date: [type:'string', pattern: /^\d{4}-\d{2}-\d{2}$/],
                    description: [type:'string'],
                    activite: [type:'string'],
                    repartition: [type:'string'],
                    direction: [type:'string'],
                    no_decision: [type:'string']
            ]
    ]

    def groovySchemaService

    /**Creates an index with proper settings and mappings*/
    def createIndex(String indexName) throws HttpHostConnectException {
        try {
            setIndexSettings(indexName)
            setIndexMapping(indexName)
        } catch (HttpHostConnectException httpexc){
            throw httpexc
        }
        print "Index "+indexName+" created"
    }

    /**Deletes the index*/
    def deleteIndex(String indexName) throws HttpHostConnectException {
        Request.Delete(SERVER_URL_ADDRESS+"/"+indexName).execute().discardContent()
        print "Index "+indexName+" deleted"
    }

    /**Indexes a contract with some specific features [Fonctionnaire]*/
    def indexFoncContract(ContractFonctionnaire contract, indexName){
        def jsonBuilder = buildJSONBodyIndexFonctionnaire(contract)
        def validationErrors = validateIndexJSONSchema(jsonBuilder)
        if (!validationErrors){
            try {
                Request.Post(SERVER_URL_ADDRESS+"/"+indexName+"/contract/")
                        .bodyString(jsonBuilder.toString(), ContentType.APPLICATION_JSON
                ).execute().returnContent().asString()
            } catch (Exception exc){
                println "Exception: "+exc.getMessage()
            }
        } else {
            println new JsonBuilder(validationErrors).toString()
        }
    }

    /**Indexes a contract with some specific features [Autre]*/
    def indexAutreContract(ContractAutre contract, indexName){
        def jsonBuilder = buildJSONBodyIndexAutre(contract)
        def validationErrors = validateIndexJSONSchema(jsonBuilder)
        if (!validationErrors){
            try {
                Request.Post(SERVER_URL_ADDRESS+"/"+indexName+"/contract/")
                        .bodyString(jsonBuilder.toString(), ContentType.APPLICATION_JSON
                ).execute().returnContent().asString()
            } catch (Exception exc){
                println "Exception: "+exc
            }
        } else {
            println new JsonBuilder(validationErrors).toString()
        }
    }

    /**Returns a json formatted response after ES research*/
    def findAllContracts(querryString){
        def jsonText = searchAllContractsWithAdvancedSearch(querryString)
        def json = new JsonSlurper().parseText(jsonText)
        def hits = json.hits
        if (hits){
            hits = hits.hits._source
            def totalHits = json.hits.total
            return new JsonBuilder([response: hits,
                    meta:[
                            error: 0,
                            total: totalHits,
                            from: querryString.from,
                            results: querryString.size
                    ]
            ]).getContent() as JSON
        } else {
            return new JsonBuilder([response: json, meta: [
                    error: 1,
                    total: 0,
                    from: querryString.from,
                    results: querryString.size
            ]]).getContent() as JSON
        }
    }

    /**Returns the actual ES query result*/
    def searchAllContractsWithAdvancedSearch(params) {
        def indexName = getIndexName(1) // Get Master Index first
        def noResponse = true
        def executionCount = 2

        if (!params.from){
            params.from = 0
        }
        if (!params.size){
            params.size = 20
        }
        if (params.startDate && !params.endDate) {
            def sdf = new SimpleDateFormat("yyyy-MM-dd")
            params.endDate = sdf.format(new Date())
        } else if (params.endDate && !params.startDate){
            params.startDate = "1970-01-01" //Unix timestamp
        }
        if (!params.startValue && params.endValue) {
            params.startValue = 0
        } else if (params.startValue && !params.endValue){
            params.endValue = Long.MAX_VALUE
        }

        def jsonBuilder
        if (params.q && params.startDate && params.endValue){
            jsonBuilder = buildCompleteJSONBody(params)
        } else if (params.q && !params.startDate && !params.endValue){
            jsonBuilder = buildJSONBodyQueryOnly(params)
        } else if (params.q && params.startDate && !params.endValue){
            jsonBuilder = buildJSONBodyQueryAndDateRange(params)
        } else if (params.q && !params.startDate && params.endValue){
            jsonBuilder = buildJSONBodyQueryAndValueRange(params)
        } else if (!params.q && params.startDate && params.endValue){
            jsonBuilder = buildJSONBodyDateAndValueRange(params)
        } else if (!params.q && params.startDate && !params.endValue){
            jsonBuilder = buildJSONBodyDateRangeOnly(params)
        } else if (!params.q && !params.startDate && params.endValue){
            jsonBuilder = buildJSONBodyValueRangeOnly(params)
        } else if (!params.q && !params.startDate && !params.endValue){
            jsonBuilder = buildJSONBodyNoParams(params)
        } else {
            jsonBuilder = null
        }

        while (noResponse){
            def url = SERVER_URL_ADDRESS + "/" + indexName + "/_search"
            def esError = [message: "", statusCode: 0]
            try {
                def response = Request.Post(url).bodyString(
                        jsonBuilder.toString(), ContentType.APPLICATION_JSON
                ).execute().returnContent().asString()
                return response
            } catch (HttpHostConnectException ignored){
                esError.message = "Service Unavailable"
                esError.statusCode = 503
                indexName = switchIndexes(indexName)
                executionCount --
            }  catch (ConnectTimeoutException ignored){
                esError.message = "Request Timeout"
                esError.statusCode = 408
                indexName = switchIndexes(indexName)
                executionCount --
            } catch (HttpResponseException exc){
                if (exc.getStatusCode() == 404){
                    esError.message = exc.getMessage()
                    esError.statusCode = exc.getStatusCode()
                    indexName = switchIndexes(indexName)
                    executionCount --
                } else {
                    return new JsonBuilder([error: exc.getMessage(), status: exc.getStatusCode()]).toString()
                }
            }
            if (executionCount  == 0){
                return new JsonBuilder([error: esError.message, status: esError.statusCode]).toString()
            }
        }
    }

    /**
     * Returns the Index name according to an id number
     * - 1: for the current DB name
     * - 2: for the temporary DB name
     * */
    def getIndexName(id){
        def databasePrefs = DatabasePrefs.find {prefId == "indexName"} as DatabasePrefs
        switch (id){
            case 1:
                databasePrefs.currentDatabaseName
                break
            case 2:
                databasePrefs.tempDatabaseName
                break
            default:
                println "Could not obtain the indexName from DataBase"
        }
    }

    /**Sets the required Index settings that allows research on case insensitive and language specific text*/
    def setIndexSettings(String indexName) throws HttpHostConnectException {
        JsonBuilder jsonBuilder = buildJSONBodyIndexSettings()
        Request.Post(SERVER_URL_ADDRESS+"/"+indexName).bodyString(
                jsonBuilder.toString(), ContentType.APPLICATION_JSON
        ).execute().discardContent()
        print "Settings: "+indexName+" set"
    }

    /**Sets the Index field mappings*/
    def setIndexMapping(String indexName) throws HttpHostConnectException {
        JsonBuilder jsonBuilder = buildJSONBodyIndexMapping()
        Request.Post(SERVER_URL_ADDRESS+"/"+indexName+"/contract/_mapping").bodyString(
                jsonBuilder.toString(), ContentType.APPLICATION_JSON
        ).execute().discardContent()
        print "Mapping: "+indexName+" set"
    }

    /**Returns the required Index name*/
    def switchIndexes(indexName){
        def res
        if (indexName == getIndexName(1)){
            res = getIndexName(2)
        } else {
            res = getIndexName(1)
        }
        return res
    }

    /**Builds the JSON body for a contract of a specific type [Fonctionnaire]*/
    def buildJSONBodyIndexFonctionnaire(ContractFonctionnaire contract){
        JsonBuilder jsonBuilder = new JsonBuilder(
                [
                        fournisseur: contract.nomFournisseur,
                        service: contract.service,
                        montant: contract.montant,
                        numero: contract.numero,
                        approbateur: contract.approbateur,
                        date: contract.date,
                        description: contract.description,
                        activite: contract.activite,
                        repartition: contract.repartition,
                        direction: null,
                        no_decision: null

                ])
        return jsonBuilder
    }

    /**Builds the JSON body for a contract of a specific type [Autre]*/
    def buildJSONBodyIndexAutre(ContractAutre contract){
        JsonBuilder jsonBuilder = new JsonBuilder(
                [
                        fournisseur: contract.nomFournisseur,
                        service: contract.service,
                        montant: contract.montant,
                        repartition: contract.repartition,
                        direction: contract.direction,
                        numero: contract.numero,
                        description: contract.description,
                        no_decision: contract.noDecision,
                        date: contract.date,
                        approbateur: null,
                        activite: null

                ])
        return jsonBuilder
    }

    /**
     * Self explanatory functions
     * */

    def validateIndexJSONSchema(JsonBuilder jsonBuilder){
        def json = jsonBuilder.getContent() as JSONObject
        return groovySchemaService.validate(json, CONTACT_SCHEMA)
    }

    def buildCompleteJSONBody(params){
        JsonBuilder jsonBuilder = new JsonBuilder()
        def root = jsonBuilder{
            from params.from
            size params.size
            query {
                filtered {
                    query {
                        multi_match {
                            query params.q
                            fields "fournisseur", "service", "approbateur", "description", "activite", "direction"
                        }
                    }
                    filter {
                        and(
                                {
                                    range {
                                        date {
                                            from params.startDate
                                            to params.endDate
                                        }
                                    }
                                },
                                {
                                    range {
                                        montant {
                                            from params.startValue
                                            to params.endValue
                                        }
                                    }
                                }
                        )
                    }
                }
            }
        }
        return jsonBuilder
    }
    def buildJSONBodyQueryOnly(params){
        JsonBuilder jsonBuilder = new JsonBuilder()
        def root = jsonBuilder{
            from params.from
            size params.size
            query {
                multi_match {
                    query params.q
                    fields "fournisseur", "service", "approbateur", "description", "activite", "direction"
                }
            }
        }
        return jsonBuilder
    }
    def buildJSONBodyDateRangeOnly(params){
        JsonBuilder jsonBuilder = new JsonBuilder()
        def root = jsonBuilder{
            from params.from
            size params.size
            query {
                filtered {
                    filter {
                        range {
                            date {
                                from params.startDate
                                to params.endDate
                            }
                        }
                    }
                }
            }
        }
        return jsonBuilder
    }
    def buildJSONBodyValueRangeOnly(params){
        JsonBuilder jsonBuilder = new JsonBuilder()
        def root = jsonBuilder{
            from params.from
            size params.size
            query {
                filtered {
                    filter {
                        range {
                            montant {
                                from params.startValue
                                to params.endValue
                            }
                        }
                    }
                }
            }
        }
        return jsonBuilder
    }
    def buildJSONBodyDateAndValueRange(params){
        JsonBuilder jsonBuilder = new JsonBuilder()
        def root = jsonBuilder{
            from params.from
            size params.size
            query {
                filtered {
                    filter {
                        and(
                                {
                                    range {
                                        date {
                                            from params.startDate
                                            to params.endDate
                                        }
                                    }
                                },
                                {
                                    range {
                                        montant {
                                            from params.startValue
                                            to params.endValue
                                        }
                                    }
                                }
                        )
                    }
                }
            }
        }
        return jsonBuilder
    }
    def buildJSONBodyQueryAndDateRange(params){
        JsonBuilder jsonBuilder = new JsonBuilder()
        def root = jsonBuilder{
            from params.from
            size params.size
            query {
                filtered {
                    query {
                        multi_match {
                            query params.q
                            fields "fournisseur", "service", "approbateur", "description", "activite", "direction"
                        }
                    }
                    filter {
                        range {
                            date {
                                from params.startDate
                                to params.endDate
                            }
                        }
                    }
                }
            }
        }
        return jsonBuilder
    }
    def buildJSONBodyQueryAndValueRange(params){
        JsonBuilder jsonBuilder = new JsonBuilder()
        def root = jsonBuilder{
            from params.from
            size params.size
            query {
                filtered {
                    query {
                        multi_match {
                            query params.q
                            fields "fournisseur", "service", "approbateur", "description", "activite", "direction"
                        }
                    }
                    filter {
                        range {
                            montant {
                                from params.startValue
                                to params.endValue
                            }
                        }
                    }
                }
            }
        }
        return jsonBuilder
    }
    def buildJSONBodyNoParams(params){
        JsonBuilder jsonBuilder = new JsonBuilder()
        def root = jsonBuilder{
            from params.from
            size params.size
            query {
                match_all {}
            }
        }
        return jsonBuilder
    }

    def buildJSONBodyIndexSettings(){
        JsonBuilder jsonBuilder = new JsonBuilder()
        def root = jsonBuilder{
            settings {
                analysis {
                    analyzer {
                        index_analyzer {
                            tokenizer "standard"
                            filter "standard", "lowercase", "stop", "asciifolding"
                        }
                        search_analyzer {
                            tokenizer "standard"
                            filter "standard", "lowercase", "stop", "asciifolding"
                        }
                    }
                }
            }
        }
        return jsonBuilder
    }
    def buildJSONBodyIndexMapping(){
        JsonBuilder jsonBuilder = new JsonBuilder([
                contract: [
                        _all: [enabled: true, index_analyzer: "index_analyzer", search_analyzer: "search_analyzer"],
                        properties: [
                                fournisseur: [
                                    type: "string",
                                    boost: 7.0,
                                    index: "analyzed",
                                    index_analyzer: "index_analyzer",
                                    search_analyzer: "search_analyzer",
                                    store: "yes"
                                ],
                                service: [
                                    type: "string",
                                    boost: 1.0,
                                    index: "analyzed",
                                    index_analyzer: "index_analyzer",
                                    search_analyzer: "search_analyzer"
                                ],
                                montant: [
                                    type: "double",
                                    index: "not_analyzed"
                                ],
                                numero: [
                                    type: "string",
                                    index: "not_analyzed"
                                ],
                                date: [
                                    type: "date",
                                    index: "not_analyzed"
                                ],
                                approbateur: [
                                    type: "string",
                                    boost: 1.0,
                                    index: "analyzed",
                                    index_analyzer: "index_analyzer",
                                    search_analyzer: "search_analyzer"
                                ],
                                activite: [
                                    type: "string",
                                    boost: 1.0,
                                    index: "analyzed",
                                    index_analyzer: "index_analyzer",
                                    search_analyzer: "search_analyzer"
                                ],
                                description: [
                                    type: "string",
                                    boost: 1.0,
                                    index: "analyzed",
                                    index_analyzer: "index_analyzer",
                                    search_analyzer: "search_analyzer"
                                ],
                                direction: [
                                    type: "string",
                                    boost: 1.0,
                                    index: "analyzed",
                                    index_analyzer: "index_analyzer",
                                    search_analyzer: "search_analyzer"
                                ],
                                repartition: [
                                    type: "string",
                                    index: "not_analyzed"
                                ],
                                no_decision: [
                                    type: "string",
                                    index: "not_analyzed"
                                ]
                        ]
                ]
        ])
        return jsonBuilder
    }
}
