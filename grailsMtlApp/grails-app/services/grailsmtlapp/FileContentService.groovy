package grailsmtlapp

import groovy.json.internal.Charsets
import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVParser
import org.apache.commons.csv.CSVRecord

import java.text.NumberFormat


class FileContentService {

    def elasticSearchGatewayService

    def downloadFile(String fileName, String fileUrl) throws Exception {
        def file = new File(fileName).newOutputStream()
        file << new URL(fileUrl).openStream()
        print "File "+fileName+" downloaded! "+new Date()
        file.close()

    }

    def readFile(String fileName, indexName, type) throws Exception {
        def counter = 1
        InputStream inputstream = new FileInputStream(fileName)
        InputStreamReader reader = new InputStreamReader(inputstream, Charsets.UTF_8)
        def csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withHeader())
        print "Reading "+fileName+"..."
        for(final csvRecord : csvParser){

            // Validating and inserting each document
            contractFactory(csvRecord, indexName, type)
            //println "line: "+counter

            /**Debuging*/
            ++counter
        }

        csvParser.close()
        reader.close()
        inputstream.close()

        print "A total of "+counter+" records found"
    }

	def deleteFile(String fileName){
        File file = new File(fileName)
        if (file.exists()){
            file.delete()
        }
        println "File "+fileName+" successfully deleted"
    }

    def createFoncContract(CSVRecord csvRecord){
        def contract = new ContractFonctionnaire()
        contract.nomFournisseur = getValue(csvRecord.get(0))
        contract.service = getValue(csvRecord.get(5))
        contract.montant = new BigDecimal(getCurrencyValue(csvRecord.get(7)))
        contract.numero = getValue(csvRecord.get(1))
        contract.approbateur = getValue(csvRecord.get(3))
        contract.date = getValue(csvRecord.get(2))
        contract.description = getValue(csvRecord.get(4))
        contract.activite = getValue(csvRecord.get(6))
        contract.repartition = getFoncRepartition(csvRecord.get(8))
        return contract
    }

    def createAutreContract(CSVRecord csvRecord, type){
        def contract = new ContractAutre(type)
        contract.nomFournisseur = getValue(csvRecord.get(0))
        contract.service = getValue(csvRecord.get(2))
        contract.montant = new BigDecimal(getCurrencyValue(csvRecord.get(8)))
        contract.repartition = getValue(csvRecord.get(1))
        contract.direction = getValue(csvRecord.get(3))
        contract.numero = getValue(csvRecord.get(4))
        contract.description = getValue(csvRecord.get(5))
        contract.noDecision = getValue(csvRecord.get(6))
        contract.date = getValue(csvRecord.get(7))
        return contract
    }

    private static double getCurrencyValue(String val){
        if (!val){
            return 0
        }
        String numberOnly = val.replaceAll("[^0-9\\,]+", "")
        NumberFormat format = NumberFormat.getInstance(Locale.FRANCE)
        Number number = format.parse(numberOnly)
        return number.doubleValue()
    }

    def getValue(String val){
        if (!val){
            return null
        }
        return val
    }

    def getFoncRepartition(String val){
        if (!val){
            return null
        }
        return "Portion AGGLO "+val
    }

    def contractFactory(csvRecord, indexName, type){
        switch (type){
            case type.fonctionnaire:
                ContractFonctionnaire contract = createFoncContract(csvRecord)
                elasticSearchGatewayService.indexFoncContract(contract, indexName)
                break
            case type.comite_executif:
                ContractAutre contractAutre = createAutreContract(csvRecord, type.comite_executif.toString())
                elasticSearchGatewayService.indexAutreContract(contractAutre, indexName)
                break
            case type.conseil_agglomeration:
                ContractAutre contractAutre = createAutreContract(csvRecord, type.conseil_agglomeration.toString())
                elasticSearchGatewayService.indexAutreContract(contractAutre, indexName)
                break
            case type.conseil_municipal:
                ContractAutre contractAutre = createAutreContract(csvRecord, type.conseil_municipal.toString())
                elasticSearchGatewayService.indexAutreContract(contractAutre, indexName)
                break
            default:
                println "Contract Factory error!"
        }
    }
}
