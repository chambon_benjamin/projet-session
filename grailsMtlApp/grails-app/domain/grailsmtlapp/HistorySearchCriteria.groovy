package grailsmtlapp

class HistorySearchCriteria {

    static constraints = {
        q blank: true, nullable: true
        startDate blank: true, nullable: true
        endDate blank: true, nullable: true
        startValue blank: true, nullable: true
        endValue blank: true, nullable: true
    }
    HistorySearchCriteria(data){
        this.q = data.q
        this.startDate = data.startDate
        this.endDate = data.endDate
        this.startValue = data.startValue
        this.endValue = data.endValue
    }

    long id
    String q
    String startDate
    String endDate
    String startValue
    String endValue
}
