package grailsmtlapp

class DatabasePrefs {
    String prefId
    String currentDatabaseName
    String tempDatabaseName

    String toString(){
        return "currentDatabaseName: "+currentDatabaseName+" tempDatabaseName: "+tempDatabaseName
    }

    static constraints = {
        currentDatabaseName blank: false, nullable: false
        tempDatabaseName blank: false, nullable: false
    }
}
