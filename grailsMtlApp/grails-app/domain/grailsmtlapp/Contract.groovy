package grailsmtlapp

abstract class Contract {

    static constraints = {
        typeContrat blank: false, nullable: false
        nomFournisseur blank: false, nullable: false
        montant blank: false, nullable: false
        numero blank: false, nullable: false
    }

    String typeContrat
    String nomFournisseur
    String service
    BigDecimal montant
    String date
    String numero
    String description
    String repartition
}
