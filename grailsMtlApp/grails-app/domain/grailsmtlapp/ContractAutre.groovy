package grailsmtlapp

class ContractAutre extends Contract {

    static constraints = {
        noDecision blank: false, nullable: false
    }

    ContractAutre(type){
        this.typeContrat = type
    }

    String direction
    String noDecision
}
