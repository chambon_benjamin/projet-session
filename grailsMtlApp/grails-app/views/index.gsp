		<!DOCTYPE html>
		<html>

		<head>
			<title>inf4375-e2015</title>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<link href="${resource(dir: 'css', file: 'bootstrap.css')}" rel="stylesheet" />	
			<link rel="shortcut icon" href="images/icons/favicon.ico" >

			<style>
				.result-number {
					color: #337AB7;
					font-weight: bold;
				}
			</style>
		</head>

		<body>
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12">
						<div class="jumbotron">
				    		<h1>Projet de session inf4375-e2015</h1>      
				    		<p>
				    			Un système qui fait l'extraction, la transformation et le stockage de données ouvertes de la Ville de Montréal.
				    		</p>
					  	</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-7">
						<div class="row">
							<div class="col-sm-12">
								<section id="ContractSearch">
									<form class="form" data-bind="submit: search">
										<div class="row">
											<div class="col-sm-2">
												<div class="form-group">
													<label for="start">Dates:</label>
													<input id="start" type="date" data-bind='value: startDate' class="form-control">
												</div>
											</div>
											<div class="col-sm-2">
												<div class="form-group">
													<label for="end">à:</label>
													<input id="end" type="date" data-bind='value: endDate'  class="form-control"/>
												</div>
											</div>
											<div class="col-sm-2">
												<div class="form-group">
													<label for="startValue">Montants:</label>
													<input id="startValue" type="number" data-bind='value: startValue' class="form-control" />
												</div>
											</div>											
											<div class="col-sm-2">												
												<div class="form-group">
													<label for="endValue">à:</label>
													<input id="endValue" type="number" data-bind='value: endValue'  class="form-control"/>
												</div>
											</div>
											<div class="col-sm-4">		
												<div class="form-group">
													<label for="searchContentInput">Mots clés:</label>	
													<div class="input-group">
														<input id="searchContentInput" type="text" data-bind='value: q' class="form-control" placeholder="Mots clés">
														<span class="input-group-btn">
												        	<button id='btnSearch' class='btn btn-primary' data-bind="click: search">Rechercher</button>
													   	</span>
													</div>	
												</div>	
											</div>		
										</div>	
									</form>
								</section>										
							</div>
						</div>	
						<div class="row">
							<div class="col-sm-12">
						      	<div class="page-header"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4">
						      	<h4><span class="result-number" data-bind="text: meta().total"></span> contrats</h4>
							</div>
							<div class="col-sm-4">
						      	<h4>max <span class="result-number" data-bind="text: meta().results"></span> affichés</h4>
							</div>
							<div class="col-sm-4">
						      	<h4>à partir de <span class="result-number" data-bind="text: meta().from"></span></h4>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
						      	<div class="page-header"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12">
								<section id="SearchResultRegion">
									<div id="grid">										
										<table class='table'>
											<thead>												
												<tr>
													<th>Nom Furnisseur:</th>
													<th>Service:</th>
													<th>Description:</th>
													<th>Date:</th>
													<th>Montant:</th>
												</tr>
											</thead>
											<tbody data-bind= "foreach: Contracts">
												<tr>
													<td class="col-sm-2" data-bind="text: fournisseur"></td>
													<td class="col-sm-2" data-bind="text: service"></td>
													<td class="col-sm-3" data-bind="text: description"></td>
													<td class="col-sm-1" data-bind="text: date"></td>
													<td class="col-sm-1" data-bind="text: montant"></td>	
												</tr>
											</tbody>											
										</table>
									</div>
								</section>
							</div>
						</div>	
						<div class="row">
							<div class="col-sm-5">
								<section id="previousSearchArea">
									<button id='btnPreviousSearch' class='btn btn-default btn-block' data-bind="click: previousPage">Precedent</button>
								</section>
							</div>
							<div class="col-sm-2"></div>
							<div class="col-sm-5">
								<section id="nextSearchArea">
									<button id='btnNextSearch' class='btn btn-default btn-block' data-bind="click: nextPage">Suivant</button>
								</section>
							</div>
						</div>								
					</div>
					<div class="col-sm-5">
						<div class="row">
							<div class="col-sm-12">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h2>L'Historique</h2>
									</div>
									<div class="panel-body">
										<section id="SearchHistoryRegion" >
											<div style="height:500px; overflow:scroll;">
												<table class='table table-striped table-hover'>
													<thead>
														<tr>
															<th>Dates:</th>
															<th> </th>
															<th>Montants:</th>
															<th> </th>
															<th>Mots clés:</th>
														</tr>	
													</thead>	
													<tbody data-bind="foreach: Records">	
														<tr data-bind="click: $root.executeRecord">
															<td class="col-sm-2" data-bind="text: startDate"></td>
															<td class="col-sm-2" data-bind="text: endDate"></td>
															<td class="col-sm-2" data-bind="text: startValue"></td>
															<td class="col-sm-2" data-bind="text: endValue"></td>
															<td class="col-sm-4" data-bind="text: q"></td>
															<td>
																<button type="button" class="btn btn-xs btn-danger" 
																		data-bind="click: $root.removeRecord">
																	Supprimer
																</button>
															</td>
														</tr>												
													</tbody>
												</table>
											</div>
										</section>
									</div>
								</div>
							</div>
						</div>
					</div>					
				</div>
				<footer class="footer">
					<div class="container">
				      	<div class="page-header"></div>
				      	<ul class="list-inline">
				      		<li>2015 </li>
				      		<li><a href="https://github.com/inf4375-e2015/projet-session-checkmate1986/blob/master/README.md">Documentation</a></li>
				      		<li>
				      			<a href="https://github.com/inf4375-e2015/projet-session-checkmate1986/blob/master/schema/schema.json">
				      				json schema
				      			</a>
				      		</li>
				      		<li><a href="https://github.com/inf4375-e2015/projet-session-checkmate1986/blob/master/raml/api.json">raml</a></li>
				      	</ul>
				    </div>
				</footer>				
			</div>	

			<g:javascript library="jquery" plugin="jquery"/>
			<g:javascript src="knockout-3.3.0.js" />
			<g:javascript src="knockout.mapping.min.js" />
			<g:javascript src="bootstrap.js" />
			<!-- <g:javascript src="bootstrap.min.js" /> -->
			<g:javascript src="bootstrap-paginator.min.js" />
			<g:javascript src="application.js" />		

			<script>
				$(document).ready(function () {
					ko.applyBindings(new ContractViewModel());
				});				
			</script>

		</body>



		</html>