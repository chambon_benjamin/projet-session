import grailsmtlapp.DatabasePrefs
import grailsmtlapp.HistorySearchCriteria
import grails.converters.JSON

class BootStrap {

    def init = { servletContext ->
        if (!DatabasePrefs.count()) {
            new DatabasePrefs(
                    prefId: "indexName",
                    currentDatabaseName: "database1",
                    tempDatabaseName: "database2")
                    .save(failOnError: true)
        }
        JSON.registerObjectMarshaller(HistorySearchCriteria) {
            def returnArray = [:]
            returnArray['id'] = it.id
            returnArray['q'] = it.q
            returnArray['startDate'] = it.startDate
            returnArray['endDate'] = it.endDate
            returnArray['startValue'] = it.startValue
            returnArray['endValue'] = it.endValue
            return returnArray
        }
    }
    def destroy = {
    }
}
