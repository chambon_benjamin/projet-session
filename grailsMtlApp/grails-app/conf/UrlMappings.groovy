class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/contracts"(controller: "contract", action: "list")

        "/userRecords/"(controller: "userHistory") {
            action = [GET: "list", POST:"save"]
        }
        "/userRecords/$id"(controller: "userHistory") {
            action = [GET:"show", PUT:"update", DELETE:"delete"]
        }

        "/"(view:"/index")
        "500"(view:'/error')
	}
}
