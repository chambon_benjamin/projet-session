package grailsmtlapp


class IndexingJob {
    def fileContentService
    def elasticSearchGatewayService
    DatabasePrefs databasePrefs
    /**
     * keeps in check every step of the following processes:
     * - Step 1: downloading each file from the given address
     * - Step 2: deleting the temporary index and recreating it anew
     * - Step 3: opening and reading each file, retrieving the content, inserting the content
     * - Step 4: temporary index becomes master index, the later becomes temporary index
     * */
    def allGood = false

    def fileUrlFonctionnaire = "http://donnees.ville.montreal.qc.ca/dataset/74efbfc7-b1bd-488f-be6f-ad122f1ebe8d/resource/a7c221f7-7472-4b01-9783-ed9e847ee8c1/download/contratsfonctionnaires.csv"
    def fileUrlCE = "http://donnees.ville.montreal.qc.ca/dataset/505f2f9e-8cec-43f9-a83a-465717ef73a5/resource/87a6e535-3a6e-4964-91f5-836cd31099f7/download/contratscomiteexecutif.csv"
    def fileUrlCA = "http://donnees.ville.montreal.qc.ca/dataset/6df93670-af44-492e-a644-72643bf58bc0/resource/35e636c1-9f99-4adf-8898-67c2ea4f8c47/download/contratsconseilagglomeration.csv"
    def fileUrlCM = "http://donnees.ville.montreal.qc.ca/dataset/6df93670-af44-492e-a644-72643bf58bc0/resource/a6869244-1a4d-4080-9577-b73e09d95ed5/download/contratsconseilmunicipal.csv"

    def fileNameFonctionnaire = "contratsfonctionnaires.csv"
    def fileNameCE = "contratscomiteexecutif.csv"
    def fileNameCA = "contratsconseilagglomeration.csv"
    def fileNameCM = "contratsconseilmunicipal.csv"
    enum ContractType{
        fonctionnaire, comite_executif, conseil_agglomeration, conseil_municipal
    }


    static triggers = {
        simple repeatInterval: 1800000 // execute job once in 30 mins
        //simple repeatInterval: 20000 // execute job once in 20 sec
    }

    def execute() {

        downloadFiles()
        refreshIndex()
        readFiles()
        switchMasterIndex()

        testDatabasePrefs() //checks the actual state of fields in the databasePrefs table
    }

    /**
     * downloading each file from the given address
     * - will stop the process if an exception is thrown, next try in 30 minutes
     * */
    def downloadFiles() {
        /**Testing broken Url: uncomment to see the effect*/
        //def fileUrlCM = "bad-gateway"

        try {
            fileContentService.downloadFile(fileNameFonctionnaire, fileUrlFonctionnaire)
            fileContentService.downloadFile(fileNameCE, fileUrlCE)
            fileContentService.downloadFile(fileNameCA, fileUrlCA)
            fileContentService.downloadFile(fileNameCM, fileUrlCM)
            allGood = true
            /**Debug*/
            println "Files successfully downloaded"
        }catch (Exception ignored){
            allGood = false
            deleteFiles()
            println "Process stopped at downloading files..."
        }
    }

    /**
     * opening and reading each file, retrieving the content, inserting the content
     * - will stop the process if an exception is thrown, next try in 30 minutes
     * */
    def readFiles(){
        /**Test: No file or Bad content, uncomment to see the effect*/
        //def fileNameCM = "contratsconseilmunicipal-bad.csv"
        if (allGood){
            try{
                fileContentService.readFile(fileNameFonctionnaire, indexName, ContractType.fonctionnaire)
                fileContentService.readFile(fileNameCE, indexName, ContractType.comite_executif)
                fileContentService.readFile(fileNameCA, indexName, ContractType.conseil_agglomeration)
                fileContentService.readFile(fileNameCM, indexName, ContractType.conseil_municipal)
                /**Debug*/
                println "Files successfully read"
                deleteFiles()
            } catch (Exception fileException){
                allGood =false
                deleteFiles()
                println "Problem found while reading one of the files "+fileException
                println "Process stopped at reading files..."
            }
        }
    }

    /**
     * deleting the temporary index and recreating it anew
     * - will stop the process if an exception is thrown, next try in 30 minutes
     * */
    def refreshIndex(){
        if (allGood){
            try{
                elasticSearchGatewayService.deleteIndex(indexName)
                elasticSearchGatewayService.createIndex(indexName)
            }catch (Exception exc){
                allGood = false
                print "ES Connection failed "+exc
                println "Process stopped at refreshing indexes..."
            }
        }
    }

    def deleteFiles(){
        fileContentService.deleteFile(fileNameFonctionnaire)
        fileContentService.deleteFile(fileNameCE)
        fileContentService.deleteFile(fileNameCA)
        fileContentService.deleteFile(fileNameCM)
    }

    /**returns the temporary Index name*/
    def getIndexName(){
        databasePrefs = DatabasePrefs.find {prefId == "indexName"} as DatabasePrefs
        databasePrefs.tempDatabaseName
    }

    /**
     * temporary index becomes master index, the later becomes temporary index
     * - the last operation
     * */
    def switchMasterIndex(){
        if (allGood){
            databasePrefs = DatabasePrefs.find {prefId == "indexName"} as DatabasePrefs
            def tempIndex = databasePrefs.currentDatabaseName
            databasePrefs.currentDatabaseName = indexName
            databasePrefs.tempDatabaseName = tempIndex
            databasePrefs.save(flush: true)
            allGood = false //End of process chain here, ready for another round
        }
    }

    def testDatabasePrefs(){
        databasePrefs = DatabasePrefs.find {prefId == "indexName"} as DatabasePrefs
        println "Current Index Name: "+databasePrefs.currentDatabaseName
        println "Temp Index Name: "+databasePrefs.tempDatabaseName
    }
}
