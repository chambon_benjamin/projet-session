package grailsmtlapp

import groovy.json.JsonSlurper

class ContractController {
    def elasticSearchGatewayService

    static allowedMethods = [list: "GET"]

    def index() {
        render "Main Index: "+elasticSearchGatewayService.getIndexName(1)+" Secondary Index: "+
                elasticSearchGatewayService.getIndexName(2)
    }

    def list() {
        def searchResult = elasticSearchGatewayService.findAllContracts(params)
        def responseContent = new JsonSlurper().parseText(searchResult.toString())
        def errorStatus = responseContent.meta.error
        if (errorStatus == 1){
            response.status = responseContent.response.status
            render(text: searchResult, contentType: 'application/json', encoding: 'utf-8')
        } else {
            render(text: searchResult, contentType: 'application/json', encoding: 'utf-8')
        }
    }
}
