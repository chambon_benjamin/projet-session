package grailsmtlapp

class HomeController {

    def elasticSearchGatewayService
	DatabasePrefs databasePrefs

    def index() {
		redirect(action: content)
	}
	
	def content = {
		render "Test: "+databasePrefs.currentDatabaseName
	}

    /**Tests*/
	def createAppleBox = {
        elasticSearchGatewayService.createIndex(databasePrefs.currentDatabaseName)
        print "db: "+databasePrefs.currentDatabaseName
    }

	def deleteAppleBox = {
        elasticSearchGatewayService.deleteIndex(databasePrefs.currentDatabaseName)
    }

	def indexApple(){
		elasticSearchGatewayService.indexApple()
	}
}
