package grailsmtlapp

import com.google.gson.JsonArray
import grails.converters.JSON
import org.apache.commons.lang.StringUtils

class UserHistoryController {

    static allowedMethods = [
            list: "GET",
            show: "GET",
            update: "PUT",
            delete: "DELETE",
            save: "POST"
    ]

    def index() {
        redirect(action: list())
    }

    def list(){
        def records = HistorySearchCriteria.list()
        if (records){
            response.status = 200
            render(text: records as JSON, contentType: 'application/json', encoding: 'utf-8')
        } else {
            response.status = 200
            render(text: new JsonArray().toString(), contentType: 'application/json', encoding: 'utf-8')
        }

    }

    def show(){
        HistorySearchCriteria record = HistorySearchCriteria.get(params.id)
        if (record != null){
            response.status = 200
            render(text: record as JSON, contentType: 'application/json', encoding: 'utf-8')
        } else {
            render status: 404
        }
    }

    def update(){
        HistorySearchCriteria record = HistorySearchCriteria.get(params.id)
        if (record != null){
            record.q = request.JSON.q
            record.startDate = request.JSON.startDate
            record.endDate = request.JSON.endDate
            record.startValue = request.JSON.startValue
            record.endValue = request.JSON.endValue
            if (record.save(flush: true)){
                response.status = 200
                render(text: record as JSON, contentType: 'application/json', encoding: 'utf-8')
            } else {
                render status: 503
            }
        } else {
            render status: 404
        }
    }

    def delete(){
        println "ID: "+params.id
        HistorySearchCriteria record = HistorySearchCriteria.get(params.id)
        if (record != null){
            record.delete(flush: true)
            render status: 204
        } else {
            render status: 404
        }
    }

    def save(){
        HistorySearchCriteria record
        if (isNotNullNotEmpty(request.JSON.q) || isNotNullNotEmpty(request.JSON.startDate) ||
                isNotNullNotEmpty(request.JSON.endDate) ||
                isNotNullNotEmpty(request.JSON.startValue) || isNotNullNotEmpty(request.JSON.endValue) ){
            record = new HistorySearchCriteria(request.JSON)
        } else {
            render status: 202
            return
        }

        if (record.save(flush: true)){
            response.status = 201
            render(text: record as JSON, contentType: 'application/json', encoding: 'utf-8')
        } else {
            render status: 503
        }
    }

    public static boolean isNotNullNotEmpty(final String string) {
        return StringUtils.isNotBlank(string);
    }
}
