	
	function isUndefined(value) {
		return typeof value === 'undefined';
	}
	
	function saveState(record) {
		var href;
		if(!isUndefined(record.q)){
			href = "http://localhost:8080/grailsMtlApp/#!/"+record.q;
		} else {
			href = "http://localhost:8080/grailsMtlApp/#!/all";
		}
		
		history.pushState(record, '', href);
	}
	
	function getContracts(record, context, statless){
		$.ajax({
			type: "GET",
			url: location.pathname+"contracts", 
			dataType: "json",
			data: record,
			contentType: "application/json",
			success: function (results) {
				console.log("search: "+results);
				var meta = results.meta;
				ko.mapping.fromJS(results.response, {}, context.Contracts);
				context.meta(new Meta(meta));
				if(!statless) {
					saveState(record);
					context.addRecord();
				}					
			}
		});
	}
	
	function Contract(data) {
		this.Furnisseur = ko.observable(data.furnisseur);
		this.Service = ko.observable(data.service);
		this.Description = ko.observable(data.description);
		this.Date = ko.observable(data.date);
		this.Montant = ko.observable(data.montant);
	}
	
	function Record(data) {
		this.id = ko.observable(data.id);
		this.q = ko.observable(data.q);
		this.startDate = ko.observable(data.startDate);
		this.endDate = ko.observable(data.endDate);
		this.startValue = ko.observable(data.startValue);
		this.endValue = ko.observable(data.endValue);
	}
	
	function Meta(data) {
		this.error = data.error;
		this.total = data.total;
		this.from = data.from;
		this.results = data.results;
	}
				
	function ContractViewModel() {
		var self = this;
		self.Contracts = ko.observableArray();
		self.meta = ko.observable({});
		self.Records = ko.observableArray();
	  
		self.q = ko.observable();		
		self.startDate = ko.observable();		
		self.endDate = ko.observable();		
        self.startValue = ko.observable();
        self.endValue = ko.observable();
					 
		self.addRecord = function () {
			 // Prevent blanks
			if ((self.q() != "" && !isUndefined(self.q())) || (self.startDate() != "" && !isUndefined(self.startDate())) || 
				(self.endDate() != "" && !isUndefined(self.endDate())) || (self.startValue() != "" && !isUndefined(self.startValue())) || 
				(self.endValue() != "" && !isUndefined(self.endValue())) ){
					
				var record = new Record({
					q: self.q(),
					startDate: self.startDate(),
					endDate: self.endDate(),
					startValue: self.startValue(),
					endValue: self.endValue()
				});
				self.currentQueryParams = record;
				self.saveRecordToDB(record);	
					
			}
			self.q("");		
			self.startDate("");		
			self.endDate("");		
			self.startValue("");
			self.endValue("");				
		};
		
		self.removeRecord = function (record) {
			self.removeRecordFromDB(record);
		};
		
		self.search = function () {	
			var record = {
				q: self.q(),
				startDate: self.startDate(),
				endDate: self.endDate(),
				startValue: self.startValue(),
				endValue: self.endValue(),
				from: 0, size: 20
			}; 		
			getContracts(record, self, false);
		}; 
		
		//Add by Shen
		self.nextPage = function () {
			console.log("next nou: ");
			var indexNextPage;
			if(self.Contracts() != null && self.Contracts().length >= 20 && self.meta() != null) {
				indexNextPage = parseInt(self.meta().from)+20;
			} else {
				return;
			}
			var record = {
				q: history.state.q,
				startDate: history.state.startDate,
				endDate: history.state.endDate,
				startValue: history.state.startValue,
				endValue: history.state.endValue,
				from: indexNextPage, size: 20
				
			}
			$.ajax({
				type: "GET",
				url: location.pathname+"contracts", 
				dataType: "json",
				data: record,
				contentType: "application/json",
				success: function (results) {
					var meta = results.meta;
					ko.mapping.fromJS(results.response, {}, self.Contracts);
					self.meta(new Meta(meta));
					saveState(record);
				}
			});
		};
		
		//Add by Shen
		self.previousPage = function () {
			var indexPreviousPage;
			if(self.Contracts() != null && self.meta() != null && self.meta().from != 0) {
				indexPreviousPage = parseInt(self.meta().from) - 20;
				if(indexPreviousPage < 0) {
					indexPreviousPage = 0;
				}
			} else {
				return;
			}
			var record = {
				q: history.state.q,
				startDate: history.state.startDate,
				endDate: history.state.endDate,
				startValue: history.state.startValue,
				endValue: history.state.endValue,
				from: indexPreviousPage, size: 20
				
			}
			$.ajax({
				type: "GET",
				url: location.pathname+"contracts", 
				dataType: "json",
				data: record,
				contentType: "application/json",
				success: function (results) {
					var meta = results.meta;
					ko.mapping.fromJS(results.response, {}, self.Contracts);
					self.meta(new Meta(meta));
					saveState(record);
				}
			});
		};
		
		self.saveRecordToDB = function (record) {
			$.ajax({
				type: "POST",
				url: location.pathname+"userRecords", 
				dataType: "json",
				data: ko.toJSON(record),
				contentType: "application/json",
				success: function (result) {
					console.log("ajax save: "+result.id);
					self.Records.push(new Record({
						id: result.id,
						q: result.q,
						startDate: result.startDate,
						endDate: result.endDate,
						startValue: result.startValue,
						endValue: result.endValue
					}));
				},
				error: function (err) {
					console.log(err.status + " - " + err.statusText);
				}
			});
		};
		
		self.removeRecordFromDB = function (record) {
			var id = record.id();
			$.ajax({
				type: "DELETE",
				url: location.pathname+"userRecords/"+id, 
				success: function (result) {
					self.Records.remove(record);
				},
				error: function (err) {
					console.log(err.status + " - " + err.statusText);
				}
			});
		};
		
		$(window).on("popstate", function() {
			var record = {
				q: history.state.q,
				startDate: history.state.startDate,
				endDate: history.state.endDate,
				startValue: history.state.startValue,
				endValue: history.state.endValue,
				from: history.state.from, 
				size: history.state.size
			}; 
			getContracts(record, self, true);
		});
		
		$.ajax({
			type: "GET",
			url: location.pathname+"contracts",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function (results) {
				var meta = results.meta;
				ko.mapping.fromJS(results.response, {}, self.Contracts);
				self.meta(new Meta(meta));
				saveState({
					q: self.q(),
					startDate: self.startDate(),
					endDate: self.endDate(),
					startValue: self.startValue(),
					endValue: self.endValue()
				});
			},
			error: function (err) {
				console.log(err.status + " - " + err.statusText);
			}
		});
		
		$.ajax({
			type: "GET",
			url: location.pathname+"userRecords",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function (results) {
				ko.mapping.fromJS(results, {}, self.Records);
			},
			error: function (err) {
				console.log(err.status + " - " + err.statusText);
			}
		})
	}	
	