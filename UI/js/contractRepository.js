var contractRepository = function() {

    function GetSearchResult(q, startDate, endDate, startValue, endValue) {

		//var _baseUrl = location.protocol + "//" + location.host;
		
        var deferred = Q.defer();

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: location.pathname+"contracts",
            data: JSON.stringify({
                q: q,
				startDate: startDate,
				endDate: endDate,
                startValue: startValue,
                endValue: endValue
            })
        }).done(function (data) {
            deferred.resolve(data);
        });

		SaveRecord(q, startDate, endDate, startValue, endValue);
        return deferred.promise;
    }
	
	function SaveRecord(q, startDate, endDate, startValue, endValue) {
                
        $.post(location.pathname+"userRecords", $.param({ q: filename, startDate: startDate, endDate: endDate, startValue: startValue, endValue: endValue}));        
    }

	function GetHistory() {
		
		var deferred = Q.defer();
		
		$.ajax({
			type: "GET",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: location.pathname+"userRecords",
			data: {}
		}).done(function (data) {
			deferred.resolve(data);
		});
		
		return deferred.promise;
	}
	
	function DeleteRecord(id) {
		
		$.ajax({
			type: "DELETE",
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			url: location.pathname+"userRecords",
			data: {id: id}
		}).done(function (data) {
			
		});
	}

    return {
        GetSearchResult: GetSearchResult,
		GetHistory: GetHistory,
		DeleteRecord: DeleteRecord
    };

}();