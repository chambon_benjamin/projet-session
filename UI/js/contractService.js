
var contractService = function () {

    var _vm = null;

    var viewModel = function () {
        var self = this;

		self.q = ko.observable('');
		
		self.startDate = ko.observable('');
		
		self.endDate = ko.observable('');
		
        self.startValue = ko.observable('');

        self.endValue = ko.observable('');
		
		self.results = ko.observable('');
		
		self.histories = ko.observable('');
		
		self.showHistoryRegion = ko.observable(false);

        self.search = SearchContract;
		
		self.searchHistory = GetHistory;
    };

    function Initialize() {
        _vm = new viewModel();
        ko.applyBindings(_vm, document.getElementById('ContractSearch'));
		ko.applyBindings(_vm, document.getElementById('SearchResultRegion'));
		ko.applyBindings(_vm, document.getElementById('SearchHistoryRegion'));
    }

    function SearchContract() {
		/*contractRepository
            .GetSearchResult(
						  _vm.q(),
						  _vm.startDate(),
						  _vm.endDate(),
                          _vm.startValue(),
                          _vm.endValue()
                          ).then(OnShowResult);*/
						  
		OnShowResult(null);
    }
	
	function OnShowResult(data) {
        data = [
				{idContrat:123, typeContrat:"fonctionnaire", nomFournisseur:"ABC", service:"serviceA", montant:1000, repartition:"test", direction:"test", noDossier:"asd123"},
			    {idContrat:123, typeContrat:"fonctionnaire", nomFournisseur:"ABC", service:"serviceA", montant:1000, repartition:"test", direction:"test", noDossier:"asd123"}
			    ];
		
		_vm.results(data);
							
    }
	
	function GetHistory() {
		/*contractRepository
            .GetHistory().then(OnShowHistory);*/
		
		OnShowHistory(null);
			
    }
	
	function OnShowHistory(data) {
		
		var data = [
						{Id:2, q:"ABC", startDate:"2018-7-1", endDate:"2015-7-9", startValue:"100", endValue:1000},
						{Id:4, q:"DEF", startDate:"2018-7-2", endDate:"2015-7-10", startValue:"200", endValue:2000},
						{Id:6, q:"GHI", startDate:"2018-7-3", endDate:"2015-7-11", startValue:"300", endValue:3000}
					];
		
		
		_vm.histories(data);
		
		_vm.showHistoryRegion(!_vm.showHistoryRegion());
    }
	
	function DeleteRecordId(rowIndex){
		var id = _vm.histories()[rowIndex].Id;
		alert("Id: "+id);
		//contractRepository.DeleteRecord(id).then(RefreshRecordGrid);  
	}
	
	function RefreshRecordGrid(){
		GetHistory();
	}

    return {
        Initialize: Initialize,
        SearchContract: SearchContract,
		GetHistory: GetHistory,
		DeleteRecordId: DeleteRecordId
    };

}();