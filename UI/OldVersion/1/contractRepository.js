var contractRepository = function() {

    var _baseUrl = location.protocol + "//" + location.host;

    function GetSearchResult(searchTypeSelected, dateDebut, dateFin, montantMin, montantMax, autorisePar, domaineActivite, motCle) {

        var deferred = Q.defer();

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: _baseUrl + "/contract",
            data: JSON.stringify({
                searchTypeSelected: searchTypeSelected,
				dateDebut: dateDebut,
				dateFin: dateFin,
                montantMin: montantMin,
                montantMax: montantMax,
                autorisePar: autorisePar,
                domaineActivite: domaineActivite,
                motCle: motCle
            })
        }).done(function (data) {
            deferred.resolve(data);
        });

        return deferred.promise;
    }

    return {
        GetSearchResult: GetSearchResult
    };

}();