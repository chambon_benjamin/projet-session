
var contractService = function () {

    var _vm = null;

    var viewModel = function () {
        var self = this;

		self.q = ko.observable('');
		
		self.startDate = ko.observable('');
		
		self.endDate = ko.observable('');
		
        self.startValue = ko.observable('');

        self.endValue = ko.observable('');
		
		self.showHistoryRegion = ko.observable(false);

        self.search = SearchContract;
		
		self.showHistory = ShowHistory;
    };

    function Initialize() {
        _vm = new viewModel();
        ko.applyBindings(_vm, document.getElementById('ContractSearch'));
		ko.applyBindings(_vm, document.getElementById('SearchHistoryRegion'));
    }

    function SearchContract() {
		contractRepository
            .GetSearchResult(
						  _vm.q(),
						  _vm.startDate(),
						  _vm.endDate(),
                          _vm.startValue(),
                          _vm.endValue()
                          ).then(OnShowResult);
						  
		OnShowResult(null);
    }
	
	function OnShowResult(data) {
        /*data = [
				{idContrat:123, typeContrat:"fonctionnaire", nomFournisseur:"ABC", service:"serviceA", montant:1000, repartition:"test", direction:"test", noDossier:"asd123"},
			    {idContrat:123, typeContrat:"fonctionnaire", nomFournisseur:"ABC", service:"serviceA", montant:1000, repartition:"test", direction:"test", noDossier:"asd123"}
			    ];*/
							
		$("#grid").kendoGrid({
			dataSource: {
				data: data,
				schema: {
					model: {
						fields: {
							idContrat: { type: "number" },
							typeContrat: { type: "string" },
							nomFournisseur: { type: "string" },
							service: { type: "string" },
							montant: { type: "number" },
							repartition: { type: "string" },
							direction: { type: "string" },
							noDossier: { type: "string" },
							details: { type: "string" },
							noDecision: { type: "string" },
							dateSignature: { type: "string" },
							noContrat: { type: "string" },
							approbateur: { type: "string" },
							dateApprobation: { type: "string" },
							description: { type: "string" },
							activite: { type: "string" },
							portion: { type: "string" }
						}
					}
				},
				pageSize: 10
			},
			height: 450,
			scrollable: true,
			sortable: true,
			filterable: true,
			pageable: {
				input: true,
				numeric: false
			},
			columns: [
				{ field: "idContrat", title: "ID Contrat", width: "200px" },
				{ field: "typeContrat", title: "Type Contrat", width: "200px" },
				{ field: "nomFournisseur", title: "Nom Fournisseur", width: "200px" },
				{ field: "service", title: "Service", width: "200px" },
				{ field: "montant", title: "Montant", width: "200px" },
				{ field: "repartition", title: "Repartition", width: "200px" },
				{ field: "direction", title: "Direction", width: "200px" },
				{ field: "noDossier", title: "No Dossier", width: "200px" },
				{ field: "details", title: "Details", width: "200px" },
				{ field: "noDecision", title: "No Decision", width: "200px" },
				{ field: "dateSignature", title: "Date Signature", width: "200px" },
				{ field: "noContrat", title: "No Contrat", width: "200px" },
				{ field: "approbateur", title: "Approbateur", width: "200px" },
				{ field: "dateApprobation", title: "Date Approbation", width: "200px" },
				{ field: "description", title: "Description", width: "200px" },
				{ field: "activite", title: "Activite", width: "200px" },
				{ field: "portion", title: "Portion", width: "200px" },
			]
		});
    }
	
	function ShowHistory() {
		_vm.showHistoryRegion(!_vm.showHistoryRegion());
    }

    return {
        Initialize: Initialize,
        SearchContract: SearchContract,
		ShowHistory: ShowHistory
    };

}();