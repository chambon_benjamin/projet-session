var contractRepository = function() {

    var _baseUrl = location.protocol + "//" + location.host;

    function GetSearchResult(q, startDate, endDate, startValue, endValue) {

        var deferred = Q.defer();

        $.ajax({
            type: "GET",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: _baseUrl + "/contracts",
            data: JSON.stringify({
                q: q,
				startDate: startDate,
				endDate: endDate,
                startValue: startValue,
                endValue: endValue
            })
        }).done(function (data) {
            deferred.resolve(data);
        });

        return deferred.promise;
    }

    return {
        GetSearchResult: GetSearchResult
    };

}();